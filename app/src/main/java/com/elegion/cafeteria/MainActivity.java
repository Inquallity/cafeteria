package com.elegion.cafeteria;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wallet.AutoResolvableVoidResult;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.CreateWalletObjectsRequest;
import com.google.android.gms.wallet.LoyaltyWalletObject;
import com.google.android.gms.wallet.Wallet;
import com.google.android.gms.wallet.WalletConstants;
import com.google.android.gms.wallet.WalletObjectsClient;
import com.google.android.gms.wallet.wobs.LoyaltyPoints;
import com.google.android.gms.wallet.wobs.LoyaltyPointsBalance;
import com.google.android.gms.wallet.wobs.TextModuleData;
import com.google.android.gms.wallet.wobs.WalletObjectsConstants;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    /* Request code */
    private static final int SAVE_TO_ANDROID = 1;

    private TextView mStatus;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_main);
        mStatus = findViewById(R.id.status_text);
        findViewById(R.id.save_to_gp).setOnClickListener(this);
    }

    @Override
    public void onClick(final View v) {
        //Generate my loyalty object
        final LoyaltyWalletObject loyaltyWalletObject = generateLoyaltyWalletObject();

        //Build 'create wallet objects' request with previously created LoyaltyWalletObject
        final CreateWalletObjectsRequest createObjectsRequest = CreateWalletObjectsRequest.newBuilder()
                .setCreateMode(CreateWalletObjectsRequest.REQUEST_IMMEDIATE_SAVE)
                .setLoyaltyWalletObject(loyaltyWalletObject)
                .build();

        //Build options for Wallet
        final Wallet.WalletOptions walletOptions = new Wallet.WalletOptions.Builder()
                .setTheme(WalletConstants.THEME_LIGHT)
                .setEnvironment(WalletConstants.ENVIRONMENT_TEST)
                .build();

        final WalletObjectsClient woc = Wallet.getWalletObjectsClient(this, walletOptions);
        final Task<AutoResolvableVoidResult> task = woc.createWalletObjects(createObjectsRequest);

        //Resolve task with helper class from library
        AutoResolveHelper.resolveTask(task, this, SAVE_TO_ANDROID);
    }

    @NonNull
    private LoyaltyWalletObject generateLoyaltyWalletObject() {
        final LoyaltyPoints points = LoyaltyPoints.newBuilder()
                .setLabel("Cafeteria points")
                .setBalance(LoyaltyPointsBalance.newBuilder().setInt(5000).build())
                .build();
        final List<TextModuleData> textModulesData = new ArrayList<>();
        final TextModuleData textModuleData = new TextModuleData("E-legion cafeteria loyalty program", "Learn and create");
        textModulesData.add(textModuleData);

        final LoyaltyWalletObject loyaltyWalletObject = LoyaltyWalletObject.newBuilder()
                .setClassId("3330105722361269189.e_legion_cafeteria")
                .setId("3330105722361269189.01_SomeObjectId_13")
                .setIssuerName("E-Legion Ltd.")
                .setProgramName("E-Legion Cafeteria")
                .setState(WalletObjectsConstants.State.ACTIVE)
                .setAccountId("1002")
                .setAccountName("Maxim Radko")
                .setBarcodeType("qrCode")
                .setBarcodeValue("e-legion.com")
                .setLoyaltyPoints(points)
                .addTextModulesData(textModulesData)
                .build();
        return loyaltyWalletObject;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SAVE_TO_ANDROID) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    mStatus.setText("Result OK");
                    break;
                case Activity.RESULT_CANCELED:
                    mStatus.setText("Result cancelled");
                    break;
                default:
                    final Status status = AutoResolveHelper.getStatusFromIntent(data);
                    if (status != null) {
                        mStatus.setText("Result " +
                                "["
                                + status.getStatusCode()
                                + "] -> ["
                                + status.getStatusMessage()
                                + "] -> ["
                                + status.getResolution()
                                + "] -> ["
                                + status.zzg()
                                + "]");
                    }
                    break;
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull final ConnectionResult connectionResult) {
        Log.d(TAG, "Connection failed " + connectionResult.toString());
    }
}
